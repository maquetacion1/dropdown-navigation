$(document).ready(function (){
    let mainNavigation= document.getElementsByClassName("main-navigation");
    let menutoggle = document.getElementsByClassName("menu-toggle");
    let dropdowns = document.getElementsByClassName("dropdown");
    $(document).on('click','.menu-toggle',function(event){
        event.preventDefault();
        // Cuando el menu se cierra
        if(mainNavigation[0].classList.contains("open")){
            mainNavigation[0].classList.remove("open");
            $(".main-navigation").css("display",'none');
            $(".main-navigation-background").css("display", "none");
            menutoggle[0].innerHTML =
            '<img src="./images/icon-menu.svg" alt="close menu button" />';
        } //Cuando abrimos el menu
        else {
            mainNavigation[0].className += " open";
            $(".main-navigation").css("display",'grid');
            $(".main-navigation-background").css("display", "block");
            menutoggle[0].innerHTML =
            '<img src="./images/icon-close-menu.svg" alt="close menu button" />';
        }
    });

    for (let i = 0; i<dropdowns.length;i++) {
        dropdowns[i].addEventListener("click", function (event) {
            event.preventDefault();
            let dropdown = document.getElementsByClassName("menu-toggle");
            // Comportamiento si se pulsa el mismo submenu
            if(!this.classList.contains("expanded")) {
                this.className += " expanded";
                $(".navigation-"+this.id).css("display",'grid');
            } else {
                $(".navigation-"+this.id).css("display",'none');
                this.classList.remove("expanded");
            }
    })}    
})
